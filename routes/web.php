<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();


Route::get('/', 'AppointmentController@index');
Route::get('/home', 'HomeController@index');
Route::get('/user_settings', 'UserSettingsController@index');
Route::post('/user_settings', 'UserSettingsController@store');


Route::get('/appointments', 'AppointmentController@index');
Route::get('appointments/create', 'AppointmentController@create');
Route::post('appointments/create', 'AppointmentController@store');
Route::get('appointments/{id}', 'AppointmentController@show')->where(['id' => '[0-9]+']);
Route::get('appointments/edit/{id}', 'AppointmentController@edit')->where(['cars_id' => '[0-9]+']);
Route::get('appointments/destroy/{id}', 'AppointmentController@destroy')->where(['cars_id' => '[0-9]+']);
Route::get('/appointments/select', 'AppointmentController@select');
Route::get('appointments/select/{id}', 'AppointmentController@selectStore')->where(['cars_id' => '[0-9]+']);
Route::get('appointments/cancel/{id}', 'AppointmentController@cancelStore')->where(['cars_id' => '[0-9]+']);

