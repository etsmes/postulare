<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('appointments', 'ApiController@getAppointments');
Route::post('appointments/select', 'ApiController@selectAppointment');
Route::post('appointments/cancel', 'ApiController@cancelAppointment');
Route::post('appointments/delete', 'ApiController@deleteAppointment');
Route::get('appointments/get', 'ApiController@getAppointment');
Route::post('appointments/store', 'ApiController@storeAppointment');
Route::get('services', 'ApiController@getServices');

Route::post('authenticate', 'AuthenticateController@authenticate');
Route::post('authenticate/user', 'AuthenticateController@getAuthenticatedUser');

//Route::get('authenticate/user', 'AuthenticateController@getAuthenticatedUser');
	
	
	