$(function() {


	var theLanguage = $('html').attr('lang');

    /*******************/
    /*  BOOTBOX HELPER */
    /*******************/
    bootbox.addLocale('ca', {
        OK: 'Ok',
        CANCEL: 'Cancel·lar',
        CONFIRM: 'Acceptar'
    });

    bootbox.setDefaults({
        locale: theLanguage
    });

    $('form.confirm').submit(function(e) {
        var currentForm = this;
        e.preventDefault();
        bootbox.confirm($(this).attr("data-confirm-message"), function(result) {
            if (result) {
                currentForm.submit();
            }
        });
    });
	
	$(".confirm-button").click(function(e){		
		e.preventDefault();
		var linkto = $(this).attr('href');		
        bootbox.confirm($(this).attr("data-confirm-message"), function(result) {
			if(result){				
				location.href= linkto;				
			}
        });		
		
	});	


	
    /****************************/
    /*    DATE TIME PICKER      */
    /****************************/
	
    $('.date.time').datetimepicker({		
		format: 'DD/MM/YYYY HH:mm',
		locale: theLanguage,
		sideBySide: true,
		showClear: true,
		showClose: true,
		ignoreReadonly: true,
		keepInvalid: true,
		useCurrent: false,
		toolbarPlacement: 'top',
		//focusOnShow: false,
		//allowInputToggle: true,
    })/*.on('dp.show', function () {
		$(this).prop('readonly', true);
	}).on('dp.hide', function () {
		$(this).prop('readonly', false);
	})*/;	
    $('.date').datetimepicker({		
		format: 'DD/MM/YYYY',
		locale: theLanguage,
		showClear: true,
		showClose: true,	
		ignoreReadonly: true,	
		keepInvalid:true,
		useCurrent: false,		
		toolbarPlacement: 'top',
		/*allowInputToggle: true,*/
    });		
	
});