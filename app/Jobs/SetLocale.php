<?php

namespace App\Jobs;

use App\Jobs\Job;
use App;

class setLocale extends Job 
{

    protected $languages;

    public function __construct()
    {
        $this->languages = config('app.languages');
    }

    public function handle()
    {
        if(!session()->has('locale'))
        {
            //session()->put('locale', \Request::getPreferredLanguage($this->languages));
			session()->put('locale', App::getLocale());			
        }

        //app()->setLocale(session('locale'));
		App::setLocale(session('locale'));
    }
}