<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use DateTime;
use App\ServiceLanguage;

class Appointment extends Model
{
	
	protected $fillable = ['id', 'start', 'end', 'service_id','comment_client','comment_professional', 'user_id_client', 'user_id_professional', 'created_at', 'updated_at', 'deleted_at'];
		
    public function setStartAttribute($value) {
		if ($value!=null){
			$datetime = DateTime::createFromFormat(config('app.datetime_formats')[config('app.locale')], $value);
			$this->attributes['start'] = $datetime->format('Y-m-d H:i');
		} else{
			$this->attributes['start'] = null;
		}
    }

    public function setEndAttribute($value) {
		if ($value!=null){
			$datetime = DateTime::createFromFormat(config('app.datetime_formats')[config('app.locale')], $value);
			$this->attributes['end'] = $datetime->format('Y-m-d H:i');
		} else{
			$this->attributes['end'] = null;
		}
    }

    public function getStartAttribute($value) {
		if ($value!=null){
			$datetime = DateTime::createFromFormat('Y-m-d H:i:s', $value);
			return $datetime->format(config('app.datetime_formats')[config('app.locale')]);
		}
    }

    public function getEndAttribute($value) {
		if ($value!=null){
			$datetime = DateTime::createFromFormat('Y-m-d H:i:s', $value);
			return $datetime->format(config('app.datetime_formats')[config('app.locale')]);
		}
    }		
		
    public function getCreatedAtAttribute() {
		if (is_null($this->attributes['created_at'])){
			return null;
		}else{		
			$datetime = DateTime::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at']);
			return $datetime->format(config('app.datetime_formats')[config('app.locale')]);
		}
	}

    public function getUpdatedAtAttribute() {
		if (is_null($this->attributes['updated_at'])){
			return null;
		}else{			
			$datetime = DateTime::createFromFormat('Y-m-d H:i:s', $this->attributes['updated_at']);
			return $datetime->format(config('app.datetime_formats')[config('app.locale')]);
		}
    }	
	
    public function getDeletedAtAttribute() {
		
		if (is_null($this->attributes['deleted_at'])){
			return null;
		}else{
			$datetime = DateTime::createFromFormat('Y-m-d H:i:s', $this->attributes['deleted_at']);
			return $datetime->format(config('app.datetime_formats')[config('app.locale')]);
		}
    }		

	public function client()
	{
		return $this->belongsTo('App\User', 'user_id_client');
	}	
	
	public function professional()
	{
		return $this->belongsTo('App\User', 'user_id_professional');
	}
	
	public function service()
	{
		return ServiceLanguage::where("service_id",$this->service_id)->where("language_id",Language::where('abbr',config('app.locale'))->first()->id)->value("name");
	}		

}
