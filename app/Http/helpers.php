<?php

function formatDateTimeView($string)
{
    return str_replace('H:i','hh:mm',str_replace('m/d/Y','mm/dd/yyyy',str_replace('d/m/Y','dd/mm/yyyy',$string)));
}