<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Appointment;
use App\ServiceLanguage;
use DB;
use Log; 
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}
    }	
	
    public function storeAppointment(Request $request)
	{
		/*$appointment = [
		'start' => $request->input('start'),
		'end' => $request->input('end'),
		'service_id' => $request->input('service_id'),
		'user_id_professional' => $request->input('user_id'),
		'comment_professional' => $request->input('comment_professional')
		];
		$id = Appointment::create($appointment)->id;	
		
		*/
		try {
			
			$res = Appointment::updateOrCreate(['id' => $request->input('id')], $request->all());	
			$res->save(); 	
			$id =$res->id;			
			
			//Log::info('appointmentId'.$id);
			return response()->json([$id], 200);			
		} catch (\Illuminate\Database\QueryException $e) {
			if (substr($e->getMessage(),0,40)=='SQLSTATE[45000]: <<Unknown error>>: 1644')
			{
				return response()->json('Aquesta data i hora està ocupada per una altra cita',409);				
			}else{
				return response()->json($e->getMessage(),409);
			}
		} 			
    }		
	
    public function getAppointment(Request $request)
	{	
		Log::info($request);
		$query = DB::table('appointments');
		$query->leftJoin('users as clients', 'appointments.user_id_client', '=', 'clients.id');
		$query->leftJoin('users as professionals', 'appointments.user_id_professional', '=', 'professionals.id');
		$query->join('services_languages', 'appointments.service_id', '=', 'services_languages.service_id');		
		$query->whereRaw("appointments.id=".$request->input('appointmentId')." AND services_languages.language_id=7");						
		$query->selectRaw("appointments.id,DATE_FORMAT(appointments.start, '%d/%m/%Y %H:%i') as start,DATE_FORMAT(appointments.end, '%d/%m/%Y %H:%i') as end,DATE_FORMAT(appointments.start, '%d/%m/%Y') as start_date,DATE_FORMAT(appointments.end, '%d/%m/%Y') as end_date,DATE_FORMAT(appointments.start, '%H:%i') as start_hour,DATE_FORMAT(appointments.end, '%H:%i') as end_hour,clients.name as client_name,professionals.name as professional_name,services_languages.service_id AS service_id,services_languages.name AS service_name,case when DATE_FORMAT(appointments.start, '%w')=0 then '".trans('messages.Sunday')."' else case when DATE_FORMAT(appointments.start, '%w')=1 then '".trans('messages.Monday')."' else case when DATE_FORMAT(appointments.start, '%w')=2 then '".trans('messages.Tuesday')."' else case when DATE_FORMAT(appointments.start, '%w')=3 then '".trans('messages.Wednesday')."' else case when DATE_FORMAT(appointments.start, '%w')=4 then '".trans('messages.Thursday')."' else case when DATE_FORMAT(appointments.start, '%w')=5 then '".trans('messages.Friday')."' else case when DATE_FORMAT(appointments.start, '%w')=6 then '".trans('messages.Saturday')."' end  end  end  end  end  end end as start_day,comment_client,comment_professional");
		
		return response()->json($query->get())->withCallback($request->input('callback'));
	}	

    public function getAppointments(Request $request)
	{	
	Log::info($request);
		//roles -> 1: Professional, 2: Client, 3: Administrador
	
		$query = DB::table('appointments');
		$query->leftJoin('users as clients', 'appointments.user_id_client', '=', 'clients.id');
		$query->leftJoin('users as professionals', 'appointments.user_id_professional', '=', 'professionals.id');
		$query->join('services_languages', 'appointments.service_id', '=', 'services_languages.service_id');		
		if ($request->input('userRole')=='1'){
			$query->whereRaw("appointments.user_id_professional=".$request->input('userId')." AND services_languages.language_id=7"/*.Language::where('abbr',config('app.locale'))->pluck('id')*/);						
		}elseif ($request->input('userRole')=='2'){
			if ($request->input('selectAppointment')=='1'){
				$query->whereRaw("appointments.user_id_client IS NULL AND services_languages.language_id=7 AND appointments.start>='".Carbon::now()."'"/*.Language::where('abbr',config('app.locale'))->pluck('id')*/);										
			}else{
				$query->whereRaw("appointments.user_id_client=".$request->input('userId')." AND services_languages.language_id=7"/*.Language::where('abbr',config('app.locale'))->pluck('id')*/);						
			}
			
		}				
		$query->selectRaw("appointments.id,DATE_FORMAT(appointments.start, '%d/%m/%Y') as start_date,DATE_FORMAT(appointments.end, '%d/%m/%Y') as end_date,DATE_FORMAT(appointments.start, '%H:%i') as start_hour,DATE_FORMAT(appointments.end, '%H:%i') as end_hour,clients.name as client_name,professionals.name as professional_name,services_languages.name AS service_name,case when DATE_FORMAT(appointments.start, '%w')=0 then '".trans('messages.Sunday')."' else case when DATE_FORMAT(appointments.start, '%w')=1 then '".trans('messages.Monday')."' else case when DATE_FORMAT(appointments.start, '%w')=2 then '".trans('messages.Tuesday')."' else case when DATE_FORMAT(appointments.start, '%w')=3 then '".trans('messages.Wednesday')."' else case when DATE_FORMAT(appointments.start, '%w')=4 then '".trans('messages.Thursday')."' else case when DATE_FORMAT(appointments.start, '%w')=5 then '".trans('messages.Friday')."' else case when DATE_FORMAT(appointments.start, '%w')=6 then '".trans('messages.Saturday')."' end  end  end  end  end  end end as start_day");
		$query->orderBy('appointments.start', 'asc');		
		//	Log::info($query);
		return response()->json($query->get())->withCallback($request->input('callback'));
	}
	
	public function selectAppointment(Request $request)
	{		
		Appointment::where('id', $request->input('appointmentId'))->update(array('user_id_client' => $request->input('userId')));
		return response()->json(['OK'], 200);		
	}	

	public function cancelAppointment(Request $request)
	{	
		//Log::info($request);
		Appointment::where('id', $request->input('appointmentId'))->update(array('user_id_client' => null));
		return response()->json(['OK'], 200);			
	}

	public function deleteAppointment(Request $request)
	{	
	
		if(Appointment::where('id', $request->input('appointmentId'))->whereNotNull('user_id_client')->exists())
		{
			return response()->json([trans('messages.Can not delete an appointment with a reservation')], 409);			
		}else{
			Appointment::destroy($request->input('appointmentId'));	
			return response()->json(['OK'], 200);
		}	
		
	}
	
    public function getServices(Request $request)
	{			
		return response()->json(ServiceLanguage::where('language_id',7)->orderBy('name')->get())->withCallback($request->input('callback'));
	}	
}
