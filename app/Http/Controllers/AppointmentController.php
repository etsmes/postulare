<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\ServiceLanguage;
use App\Language;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class AppointmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }	
	
	public function show($id)
	{				
		return view('appointment.detail')->with(['appointment' => Appointment::find($id)]);
	}	
	
	public function create()
	{		
		return view('appointment.form')->with(['formViewMode' => 'create','dateTimeFormat' => config('app.datetime_formats')[config('app.locale')],'services'=>ServiceLanguage::where('language_id',Language::where('abbr',config('app.locale'))->pluck('id'))->orderBy('name')->pluck('name', 'service_id')]);
	}	
	
    public function index()
	{	
		$user = Auth::user();
		if ($user->hasRole('Administrador'))
		{
			return view('appointment.list')->with(['formViewMode' => 'list','appointments' => Appointment::orderBy('start', 'asc')->get()]);		
		}elseif ($user->hasRole('Professional')) {
			return view('appointment.list')->with(['formViewMode' => 'list','appointments' => Appointment::where('user_id_professional', $user->id)->orderBy('start', 'asc')->get()]);				
		}else{
			return view('appointment.list')->with(['formViewMode' => 'list','appointments' => Appointment::where('user_id_client', $user->id)->orderBy('start', 'asc')->get()]);							
		}
		
	}	
	
    public function select()
	{	
		return view('appointment.list')->with(['formViewMode' => 'select','appointments' => Appointment::where('user_id_client', null)->whereRaw(" appointments.start>='".Carbon::now()."' ")->orderBy('start', 'asc')->get()]);		
	}		
	
	public function selectStore($id)
	{	
		Appointment::where('id', $id)->update(array('user_id_client' => Auth::user()->id));
		session()->flash('alert-success',  trans('messages.Appointment').' '.trans('messages.selected successfully f'));
		return redirect('appointments');			
	}	

	public function cancelStore($id)
	{	
		Appointment::where('id', $id)->update(array('user_id_client' => null));
		session()->flash('alert-success',  trans('messages.Appointment').' '.trans('messages.cancelled successfully f'));
		return redirect('appointments');			
	}	
	
	public function edit($id)
	{	
		return view('appointment.form')->with(['formViewMode' => 'edit','dateTimeFormat' => config('app.datetime_formats')[config('app.locale')],'services'=>ServiceLanguage::where('language_id',Language::where('abbr',config('app.locale'))->pluck('id'))->orderBy('name')->pluck('name', 'service_id'),'appointment' => Appointment::find($id)]);			
	}		
	
	public function store(Request $request)
	{
		if (Auth::user()->hasRole('Client'))
		{
			
		}else{
			if ($request->input('id')==null or Appointment::where('id', $request->input('id'))->where('user_id_client',null)->exists())
			{
				$this->validate($request, ['start' => 'required|date_format:'.config('app.datetime_formats')[config('app.locale')],'end' => 'required|date_format:'.config('app.datetime_formats')[config('app.locale')].'|after:start']);
			}
		}			
		
		try {
			$appointment = Appointment::updateOrCreate(['id' => $request->input('id')], $request->all());		
			$appointment->save(); 	
			$id =$appointment->id;
			$request->session()->flash('alert-success', trans('messages.Appointment').' '.trans('messages.stored successfully f'));
			return redirect('appointments/'.$id);		
		} catch (\Illuminate\Database\QueryException $e) {
			if (substr($e->getMessage(),0,40)=='SQLSTATE[45000]: <<Unknown error>>: 1644')
			{
				$request->session()->flash('alert-danger', 'Aquesta data i hora està ocupada per una altra cita');
				return Redirect::back()->withInput(Input::all());
			}else{				
				$request->session()->flash('alert-danger', $e->getMessage());
				return Redirect::back()->withInput(Input::all());				
			}
		} 		

	}	
	
	public function destroy($id)
	{
		if(Appointment::where('id', $id)->whereNotNull('user_id_client')->exists())
		{
			session()->flash('alert-danger',  trans('messages.Can not delete an appointment with a reservation'));			
		}else{
			Appointment::destroy($id);	
			session()->flash('alert-success',  trans('messages.Appointment').' '.trans('messages.deleted successfully f'));			
		}

		return redirect()->back();	
		
	}	
	
}

