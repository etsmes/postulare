<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\RoleUser;
use Auth;
use Log;
class AuthenticateController extends Controller
{
 
    public function __construct()
   {	  

		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}
		//$this->middleware('auth', ['except' => ['authenticate']]);
   }
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "Auth index";
    }
 
    public function authenticate(Request $request)
	{
		$credentials = $request->only('email', 'password');
		 if (Auth::attempt($credentials))
		 {
			 $token = Auth::user()->remember_token;
			 return response()->json(compact('token'));			 
		 }else
		 {
			 return response()->json(['user_not_found'], 404);
		 }

    }	

    public function getAuthenticatedUser(Request $request)
    {	
		$credentials = $request->only('email', 'password');
		//Log::info($credentials);
		 if (Auth::attempt($credentials))
		 {
			 $user = Auth::user();			 
			 $user->roleId = RoleUser::where("user_id",$user->id)->first()->role_id;
			 return response()->json(compact('user'));		 
		 }else
		 {
			 return response()->json(['user_not_found'], 404);
		 }	

    }

}