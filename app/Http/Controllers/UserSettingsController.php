<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Jobs\ChangeLocale;

class UserSettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user_settings');
    }
	
	public function store(Request $request)
	{
        $changeLocale = new ChangeLocale($request->input('lang'));
        $this->dispatch($changeLocale);
		$request->session()->flash('alert-success', trans('messages.Settings').' '.trans('messages.stored successfully f'));
        return redirect()->back();		

	}		
}
