@extends('layouts.app')

@section('content')
<div class="container">
	@include('flash_message')	
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-primary">
				<div class="panel-heading"><i class="fa fa-cog"></i> {{ trans('messages.Settings') }} </div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ action('UserSettingsController@store') }}">					
					   <div class="form-group">
							<label for="language" class="col-md-4 control-label">{{ trans('messages.Language') }}</label>
							<div class="col-md-6">
								<select name="lang" class="form-control">
								  <option value="ca" {{ session('locale') == 'ca' ? 'selected' : '' }}>Catal&agrave;</option>
								  <option value="es" {{ session('locale') == 'es' ? 'selected' : '' }}>Espa&ntildeol</option>
								  <option value="en" {{ session('locale') == 'en' ? 'selected' : '' }}>English</option>							  
								</select>																				
							</div>		
						</div>	
					    <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-success">
								<i class="glyphicon glyphicon-ok"></i> {{ trans('messages.Save') }}
								</button>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">	
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection