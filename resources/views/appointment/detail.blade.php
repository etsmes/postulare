@extends('layouts.app')

@section('content')
<div class="container">
	@include('flash_message')
	<div class="row">
		<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
			<div class="panel panel-primary">
				<div class="panel-heading"><i class="fa fa-calendar-o"></i>
					{{ trans('messages.Show') }} {{ trans('messages.Appointment') }}
				</div>
				<div class="panel-body">
					<div class="row">
						<label class="control-label col-sm-2 text-right">{{ trans('messages.Start') }}</label>
						<div class="col-sm-10">
							<?=$appointment['start']?>
						</div>
					</div>
					<div class="row">
						<label class="control-label col-sm-2 text-right">{{ trans('messages.End') }}</label>
						<div class="col-sm-10">
							<?=$appointment['end']?>
						</div>
					</div>
					<div class="row">
						<label class="control-label col-sm-2 text-right">{{ trans('messages.Service') }}</label>
						<div class="col-sm-10">
							{{ $appointment->service() }}
						</div>
					</div>	
					
					<div class="row">
						<label class="control-label col-sm-2 text-right">{{ trans('messages.Professional') }}</label>
						<div class="col-sm-10">
							{{ $appointment->professional->name }}
						</div>
					</div>	
					<div class="row">
						<label class="control-label col-sm-2 text-right">{{ trans('messages.Comment_Professional') }}</label>
						<div class="col-sm-10">
							<?=$appointment['comment_professional']?>
						</div>
					</div>						
					<div class="row">
						<label class="control-label col-sm-2 text-right">{{ trans('messages.Client') }}</label>
						<div class="col-sm-10">
							@if (isset($appointment['user_id_client'])){{ $appointment->client->name }}@endif
						</div>
					</div>	
					<div class="row">
						<label class="control-label col-sm-2 text-right">{{ trans('messages.Comment_Client') }}</label>
						<div class="col-sm-10">
							<?=$appointment['comment_client']?>
						</div>
					</div>
					
					<br />
					<div class="row">
						<label class="control-label col-sm-2 text-right">&nbsp;</label>
						<div class="col-sm-10">
							<a class="btn btn-default btn-close" href="javascript:window.history.back();"><i class="glyphicon glyphicon-arrow-left"></i> {{ trans('messages.Appointments') }}</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

