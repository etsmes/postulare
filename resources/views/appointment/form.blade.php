@extends('layouts.app')

@section('content')
<div class="container">
	@include('flash_message')
	<div class="row">
		<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
			<div class="panel panel-primary">
				<div class="panel-heading"><i class="fa fa-calendar-o"></i>
					@if ($formViewMode =='create')
					{{ trans('messages.Add') }}
					@elseif ($formViewMode =='edit')
					{{ trans('messages.Edit') }}
					@endif
					{{ trans('messages.Appointment') }}			
				</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" action="{{ action('AppointmentController@store') }}" method="post">
						{{ csrf_field() }}
						<input id="id" name="id" type="hidden" value="{{old('id',isset($appointment['id']) ? $appointment['id'] : null)}}" />	
						@if (Auth::user()->hasRole(['Professional', 'Administrador']))						
						<input id="user_id_professional" name="user_id_professional" type="hidden" value="{{ Auth::user()->id }}" />						
						@endif
						@if (Auth::user()->hasRole(['Client', 'Administrador']))		
						<input id="user_id_client" name="user_id_client" type="hidden" value="{{ Auth::user()->id }}" />						
						@endif						
						@if (Auth::user()->hasRole(['Professional', 'Administrador']) and !isset($appointment['user_id_client'])) 					
						<div class="form-group{{ $errors->has('start') ? ' has-error' : '' }}">
							<label class="control-label col-sm-2" for="start">{{ trans('messages.Start') }}</label>
							<div class="col-sm-10">
								<div class="input-group date time">
									<input type="text" id="start" placeholder="{{ formatDateTimeView($dateTimeFormat)  }}" name="start" class="form-control" value="{{old('start',isset($appointment['start']) ? $appointment['start'] : null)}}" /><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
								</div>
								@if ($errors->has('start'))
								<span class="help-block">
									<strong>{{ formatDateTimeView($errors->first('start'))  }}</strong>
								</span>
								@endif
							</div>
						</div>		
						<div class="form-group{{ $errors->has('end') ? ' has-error' : '' }}">
							<label class="control-label col-sm-2" for="end">{{ trans('messages.End') }}</label>
							<div class="col-sm-10">
								<div class="input-group date time">
									<input type="text" id="end" placeholder="{{ formatDateTimeView($dateTimeFormat)  }}" name="end" class="form-control" value="{{old('end',isset($appointment['end']) ? $appointment['end'] : null)}}" /><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
								</div>
								@if ($errors->has('end'))
								<span class="help-block">
									<strong>{{ formatDateTimeView($errors->first('end'))  }}</strong>
								</span>
								@endif
							</div>
						</div>	
						<div class="form-group">
							<label class="control-label col-sm-2" for="services">{{ trans('messages.Service') }}</label>
							<div class="col-sm-10">
								<select class="form-control" name="service_id">
									@foreach($services as $service_id => $name)
									<option {{old('service_id',isset($appointment['service_id']) ? $appointment['service_id'] : 0)==$service_id ? 'selected' : ''}} value="{{$service_id}}">{{$name}}</option>
									@endforeach
								</select>
							</div>
						</div>	
											
						@else
						<div class="row">
							<label class="control-label col-sm-2">{{ trans('messages.Start') }}</label>
							<div class="col-sm-10">
								<?=$appointment['start']?>
							</div>
						</div>
						<div class="row">
							<label class="control-label col-sm-2">{{ trans('messages.End') }}</label>
							<div class="col-sm-10">
								<?=$appointment['end']?>
							</div>
						</div>	
						<div class="row">
							<label class="control-label col-sm-2">{{ trans('messages.Service') }}</label>
							<div class="col-sm-10">
								{{ $appointment->service() }}
							</div>
						</div>	
						<div class="row">
							<label class="control-label col-sm-2 text-right">{{ trans('messages.Professional') }}</label>
							<div class="col-sm-10">
								{{ $appointment->professional->name }}
							</div>
						</div>						
						@endif
						
						
						<div class="form-group">
							<label class="control-label col-sm-2" for="comment_professional">{{ trans('messages.Comment_Professional') }}</label>
							<div class="col-sm-10">
								@if (Auth::user()->hasRole(['Professional', 'Administrador']))									
								<textarea rows="4" class="form-control" id="comment_professional" name="comment_professional">{{old('comment_professional',isset($appointment['comment_professional']) ? $appointment['comment_professional'] : null)}}</textarea>
								@else
									@if (isset($appointment['comment_client']))	<?=$appointment['comment_professional']?> @endif
								@endif									
							</div>
						</div>	
		
						@if ($formViewMode =='edit')			
						<div class="row">
							<label class="control-label col-sm-2 text-right">{{ trans('messages.Client') }}</label>
							<div class="col-sm-10">
								@if (isset($appointment['user_id_client'])) {{ $appointment->client->name }} @endif
							</div>
						</div>	
													
																				
						<div class="form-group">
							<label class="control-label col-sm-2" for="comment_client">{{ trans('messages.Comment_Client') }}</label>
							<div class="col-sm-10">
								@if (Auth::user()->hasRole(['Professional'])) 	
									@if (isset($appointment['comment_client']))	<?=$appointment['comment_client']?> @endif
								@else
									<textarea rows="4" class="form-control" id="comment_client" name="comment_client">{{old('comment_client',isset($appointment['comment_client']) ? $appointment['comment_client'] : null)}}</textarea>
								@endif	
							</div>
						</div>
						@endif									
											<br />
						<div class="form-group">
							<label class="control-label col-sm-2" >&nbsp;</label>
							<div class="col-sm-10">
								<button type="submit" class="btn btn-success">
									<i class="glyphicon glyphicon-ok"></i> {{ trans('messages.Save') }}
								</button>
								@if ($formViewMode =='edit' and Auth::user()->hasRole(['Client','Administrador']) and !isset($appointment['user_id_client'])) <a class="btn btn-success" title="{{trans('messages.Select')}}" href="{{ URL::to('/appointments/select') }}/{{ $appointment->id }}"><span class="fa fa-check-square"></span> {{trans('messages.Select')}}</a>@endif								
								@if ($formViewMode =='edit' and Auth::user()->hasRole(['Client','Administrador']) and isset($appointment['user_id_client'])) <a class="btn btn-danger" title="{{trans('messages.Cancel')}}" href="{{ URL::to('/appointments/cancel') }}/{{ $appointment->id }}"><span class="fa fa-remove"></span> {{trans('messages.Cancel')}}</a>@endif								
								<a class="btn btn-default btn-close" href="{{ URL::to('/appointments') }}"><i class="glyphicon glyphicon-arrow-left"></i> {{ trans('messages.Appointments') }}</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection