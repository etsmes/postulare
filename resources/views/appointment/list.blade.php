@extends('layouts.app')

@section('content')

<div class="container">
	@include('flash_message')

	@if ($formViewMode =='select')	
		<a class="btn btn-default btn-close" href="{{ URL::to('/appointments') }}"><i class="glyphicon glyphicon-arrow-left"></i> {{ trans('messages.My appointments') }}</a>
	@else 		
		@hasrole(['Professional', 'Administrador'])
			<a class="btn btn-large btn-primary" href="{{ URL::to('/appointments/create') }}"><i class="glyphicon glyphicon-plus"></i> {{ trans('messages.Add') }} {{ trans('messages.Appointment') }}</a>
		@endhasrole
		@hasrole(['Client', 'Administrador'])
			<a class="btn btn-large btn-primary" href="{{ URL::to('/appointments/select') }}"><i class="glyphicon glyphicon-plus"></i> {{ trans('messages.Select') }} {{ trans('messages.Appointment') }}</a>
		@endhasrole	
	@endif	
		<br />
	<br />
	


		<?php 
			$odd=true; 
			$startTmp=null;			
		?>		
		@foreach ($appointments as $appointment)
			@if ($startTmp!=Carbon\Carbon::parse($appointment->getOriginal('start'))->format('d/m/Y'))
			@if ($startTmp!=null)
			<?php 
				$odd=!$odd; 				
			?>				
			</div>	
			@endif
			<div class="panel panel-{{ ($odd) ? 'primary' : 'danger' }}">
			<div class="panel-heading" style="{{ ($odd) ? '' : 'background-color:#a94442;color:white;' }}">
				{{trans('messages.'.Carbon\Carbon::parse($appointment->getOriginal('start'))->format('l') )}}	{{ Carbon\Carbon::parse($appointment->getOriginal('start'))->format('d/m/Y') }}
			</div>
			@else
				<hr style="border-top: 1px dotted;height:0px;font-size:0px;margin:0px;padding:0px;"/>
			@endif
			<div class="panel-body bg-{{ ($odd) ? 'info' : 'danger' }}">
				{{ Carbon\Carbon::parse($appointment->getOriginal('start'))->format('H:i') }} - {{ Carbon\Carbon::parse($appointment->getOriginal('end'))->format('H:i') }}
				<br>
				@if ($appointment->client!=null)
				{{ $appointment->client->name }}
				<br>			
				@endif						
				@if ($appointment->professional!=null)
				{{ $appointment->professional->name }}
				<br>			
				@endif
				{{ $appointment->service() }}
				<br>
				<a class="btn btn-info btn-xs" title="{{trans('messages.Show')}}" href="{{ URL::to('/appointments') }}/{{ $appointment->id }}"><span class="glyphicon glyphicon-asterisk"></span> {{trans('messages.Show')}}</a>
				<a class="btn btn-primary btn-xs" title="{{trans('messages.Edit')}}" href="{{ URL::to('/appointments/edit') }}/{{ $appointment->id }}"><span class="glyphicon glyphicon-pencil"></span> {{trans('messages.Edit')}}</a>									
				@hasrole(['Professional', 'Administrador'])
				
				<a class="btn btn-danger btn-xs confirm-button" title="{{trans('messages.Delete')}}" href="{{ URL::to('/appointments/destroy') }}/{{ $appointment->id }}" data-confirm-message="{{ trans('messages.Are you sure you want to delete this item?')}}"><span class="glyphicon glyphicon-trash"></span> {{trans('messages.Delete')}}</a>													
				@endhasrole
				@hasrole(['Client', 'Administrador'])
					@if ($appointment->user_id_client==null)
						<a class="btn btn-success btn-xs" title="{{trans('messages.Select')}}" href="{{ URL::to('/appointments/select') }}/{{ $appointment->id }}"><span class="fa fa-check-square"></span> {{trans('messages.Select')}}</a>													
					@else						
						<a class="btn btn-danger btn-xs" title="{{trans('messages.Cancel')}}" href="{{ URL::to('/appointments/cancel') }}/{{ $appointment->id }}"><span class="fa fa-remove"></span> {{trans('messages.Cancel')}}</a>													
					@endif
				@endhasrole				
			</div>	
			<?php 				
				$startTmp = Carbon\Carbon::parse($appointment->getOriginal('start'))->format('d/m/Y'); 
			?>				
		@endforeach
	

</div>



@endsection
