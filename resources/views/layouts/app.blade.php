<!DOCTYPE html>
<html lang="{{ $lang }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Postulare') }}</title>

	<script src="{{ url('/js/jquery-1.12.4.min.js') }}"></script>		
	<link href="{{ url('/bootstrap-3.3.7/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ url('/bootstrap-datetimepicker-4.17.42/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">	
	
    <!-- Styles -->
	<link rel="stylesheet" href="{{ url('/font-awesome-4.6.3/css/font-awesome.min.css') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/postulare.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}" title="{{ trans('messages.Home') }}">
                        {{ config('app.name', 'Postulare') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <!--ul class="nav navbar-nav">
                        &nbsp;
                    </ul-->

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">{{ trans('auth.Login') }}</a></li>
                            <li><a href="{{ route('register') }}">{{ trans('auth.Register') }}</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} @if (Auth::user()->hasRole(['Administrador'])) (Administrador) @endif @if (Auth::user()->hasRole(['Professional'])) (Professional) @endif	@if (Auth::user()->hasRole(['Client'])) (Client) @endif<span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
									<li>
										<a href="{{ url('/') }}" title="{{ trans('messages.Home') }}">
											<i class="fa fa-home"></i> {{ trans('messages.Home') }}
										</a>								
									</li>											
									@if (Auth::user()->hasRole(['Administrador'])) 
									<li>									
										<a href="{{ url('/admin') }}">
                                            <i class="fa fa-tachometer"></i> {{ trans('messages.Admin_panel') }}
                                        </a>
									</li>
									@endif	
									<li>									
										<a href="{{ url('/user_settings') }}">
                                            <i class="fa fa-cog"></i> {{ trans('messages.Settings') }}
                                        </a>
									</li>									
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out"></i> {{ trans('auth.Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->    
	<script src="{{ url('/js/moment-2.14.1/moment.min.js') }}"></script>
	<script src="{{ url('/js/moment-2.14.1/locale/'.$lang.'.js') }}"></script>
	<script src="{{ url('/bootstrap-3.3.7/js/bootstrap.min.js') }}"></script>
	<script src="{{ url('/bootstrap-datetimepicker-4.17.42/bootstrap-datetimepicker.min.js') }}"></script>	
	<script src="{{ url('/js/bootbox.min.js') }}"></script>	
	<script src="{{ url('/js/postulare.js') }}"></script>	
	
</body>
</html>
