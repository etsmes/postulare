<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

	'added successfully' => 'a�adido correctamente',
	'added successfully f' => 'a�adida correctamente',	
	'Add'=>'A�adir',
    'Admin_panel' => "Panel de administraci&#243;n",
	'Appointment'=>'Cita',
	'Appointments'=>'Citas',	
	'Are you sure you want to delete this item?' => '�Seguro que quieres eliminar este registro?',	
	'cancelled successfully' => 'cancelada correctamente',
	'cancelled successfully f' => 'cancelado correctamente',
    'Can not delete an appointment with a reservation' => 'No se puede eliminar una cita reservada',	
	'Cancel' => 'Cancelar',	
	'Client' => 'Cliente',	
	'Comment_Client'=>'Observaciones del cliente',
	'Comment_Professional'=>'Observaciones del profesional',
	'deleted successfully' => 'eliminado correctamente',
	'deleted successfully f' => 'eliminada correctamente',	
	'Delete' => 'Eliminar',	
	'Edit'=>'Modificar',	
	'End'=>'Fin',	
	'Home'=>'Inicio',		
	'Language'=>'Idioma',	
	'My appointments'=>'Mis citas',	
	'Professional' => 'Profesional',		
	'Save' => 'Guardar',
	'selected successfully' => 'seleccionado correctamente',
	'selected successfully f' => 'seleccionada correctamente',		
	'Select' => 'Seleccionar',	
	'Service'=>'Servicio',
	'Settings'=>'Configuraci&#243;n',	
	'Show' => 'Mostrar',	
	'Start'=>'Inicio',	
	'stored successfully' => 'almacenado correctamente',
	'stored successfully f' => 'almacenada correctamente',	
	
    'Monday' => 'Lunes',
	'Tuesday' => 'Martes',
	'Wednesday' => 'Mi�rcoles',
	'Thursday' => 'Jueves',
	'Friday' => 'Viernes',
	'Saturday' => 'S�bado',
	'Sunday' => 'Domingo',		

];


