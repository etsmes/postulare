<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'Confirm' => 'Confirmar',
    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'Forgot Your Password?' => '¿Olvidó su contraseña?',
	'Login' => 'Iniciar sessión',
	'Logout' => 'Cerrar sesión',	
	'Name' => 'Nombre',
	'Password' => 'Contraseña',
	'Register' => "Register",
	'Remember Me' => "Recuérdame",
	'Reset password' => "Reinicar contraseña",
	'Send Password Reset Link' => "Enviar enlace para rehacer la contraseña",
	'throttle' => 'Demasiados intentos de inicio de sesión. Por favor, inténtelo de nuevo en :seconds segundos',
	
	

];
