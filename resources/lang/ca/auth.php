<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'Confirm' => 'Confirmar',
    'failed' => 'Aquestes credencials no coincideixen amb els nostres registres.',
    'Forgot Your Password?' => 'Heu oblidat la constrasenya?',
	'Login' => 'Iniciar sessió',
	'Logout' => 'Tancar sessió',	
	'Name' => 'Nom',
	'Password' => 'Contrasenya',
	'Register' => "Registrar-se",
	'Remember Me' => "Recorda'm",
	'Reset password' => "Refer contrasenya",
	'Send Password Reset Link' => "Enviar enllaç per refer la contrasenya",
	'throttle' => 'Massa intents de connexió. Intenteu-ho de nou en :seconds segons.',
	

];

