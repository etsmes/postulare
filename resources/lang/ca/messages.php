<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

	'added successfully' => 'afegit correctament',
	'added successfully f' => 'afegida correctament',	
	'Add'=>'Afegir',
    'Admin_panel' => "Panell d'administraci&#243;",
	'Appointment'=>'Cita',	
	'Appointments'=>'Cites',
	'Are you sure you want to delete this item?' => 'Segur que vols eliminar aquest registre?',
	'cancelled successfully f' => 'cancel·lada correctament',
	'cancelled successfully' => 'cancel·lat correctament',
	'Can not delete an appointment with a reservation' => 'No es pot eliminar una cita reservada',
	'Cancel' => 'Cancel·lar',	
	'Client' => 'Client',	
	'Comment_Client'=>'Observacions del client',
	'Comment_Professional'=>'Observacions del professional',	
	'deleted successfully' => 'eliminat correctament',
	'deleted successfully f' => 'eliminada correctament',	
	'Delete' => 'Eliminar',	
	'Edit'=>'Modificar',	
	'End'=>'Fi',		
	'Home'=>'Inici',		
	'Language'=>'Idioma',	
	'My appointments'=>'Les meves cites',	
	'Professional' => 'Professional',		
	'Save' => 'Desar',	
	'selected successfully' => 'seleccionat correctament',
	'selected successfully f' => 'seleccionada correctament',			
	'Select' => 'Seleccionar',	
	'Service'=>'Servei',
	'Settings'=>'Configuració',
	'Show' => 'Mostrar',
	'stored successfully' => 'emmagatzemat correctament',
	'stored successfully f' => 'emmagatzemada correctament',
	'Start'=>'Inici',	
	
	    
    'Monday' => 'Dilluns',
	'Tuesday' => 'Dimarts',
	'Wednesday' => 'Dimecres',
	'Thursday' => 'Dijous',
	'Friday' => 'Divendres',
	'Saturday' => 'Dissabte',
	'Sunday' => 'Diumenge',	    	

];


