<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Permission Manager Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Laravel Backpack - Permission Manager
    | Author: Lúdio Oliveira <ludio.ao@gmail.com>
    |
    */
    'name'                  => "Nom",
    'role'                  => "Rol",
    'roles'                 => "Rols",
    'roles_have_permission' => "Rols que tenen aquest permís",
    'permission_singular'   => "permís",
    'permission_plural'     => "permisos",
    'user_singular'         => "Usuari",
    'user_plural'           => "Usuaris",
    'email'                 => "Email",
    'extra_permissions'     => "Permisos addicionals",
    'password'              => "Contrasenya",
    'password_confirmation' => "Confirmació de Contrasenya",
    'user_role_permission'  => "Permisos de rol d'usuari",
    'user'                  => "Usuari",
    'users'                 => "Usuaris",

];
