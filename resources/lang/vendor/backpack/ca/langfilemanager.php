<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack\LangFileManager Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD interface for lang files.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'active'                 => "Actiu",
    'cant_edit_online'       => "Aquest fitxer d'idioma no pot ser editat en línia.",
    'code_iso639-1'          => "Code (ISO 639-1)",
    'default'                => "Defecte",
    'empty_file'             => "Traducció no disponible.",
    'flag_image'             => "Imatge de la bandera",
    'key'                    => "Clau",
    'language'               => "idioma",
    'language_name'          => "Nom de l'idioma",
    'language_text'          => ":language_name text",
    'language_translation'   => ":language_name traducció",
    'languages'              => "idiomes",
    'please_fill_all_fields' => "Si us plau, introdueixi tots els camps",
    'rules_text'             => "<strong>Avis: </strong>No traduir paraules amb prefix de dos punts (ex: ': number_of_items'). Aquests seran substituïts automàticament amb un valor apropiat. Si es tradueix, deixa de funcionar.",
    'saved'                  => "Desat",
    'site_texts'             => "Text del lloc",
    'switch_to'              => "Canviar a",
    'texts'                  => "Textos",
    'translate'              => "Traduir",
    'translations'           => "Traduccions",
    'native_name'            => "Nom nadiu",

];
