<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack Crud Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD interface.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    // Forms
    'save_action_save_and_new' => 'Guardar i crear nou',
    'save_action_save_and_edit' => 'Guardar i continuar editant',
    'save_action_save_and_back' => 'Guardar i tornar',
    'save_action_changed_notification' => "L'acció por defecte del botó guardar ha estat modificada.",

    // Create form
    'add'                 => 'Afegir',
    'back_to_all'         => 'Tornar al llistat de',
    'cancel'              => 'Cancel·lar',
    'add_a_new'           => 'Afegir ',

    // Edit form
    'edit'                 => 'Editar',
    'save'                 => 'Guardar',

    // Revisions
    'revisions'            => 'Les revisions',
    'no_revisions'         => "No s'han trobat revisions ",
    'created_this'         => 'creat aquest',
    'changed_the'          => 'canviat el',
    'restore_this_value'   => 'restaurar aquest valor',
    'from'                 => 'de',
    'to'                   => 'a',
    'undo'                 => 'Desfer',
    'revision_restored'    => 'Revisió restaurada correctament',

    // CRUD table view
    'all'                       => 'Tots els registres de ',
    'in_the_database'           => 'a la base de dades',
    'list'                      => 'Llistar',
    'actions'                   => 'Accions',
    'preview'                   => 'Vista prèvia',
    'delete'                    => 'Eliminar',
    'admin'                     => 'Admin',
    'details_row'               => 'Aquesta és la fila de detalls. Modificar al seu gust.',
    'details_row_loading_error' => "S'ha produit un error al carregar les dades. Per favor, intenti-ho de nou.",

    // Confirmation messages and bubbles
    'delete_confirm'                              => 'Està segur que desitja eliminar aquest element?',
    'delete_confirmation_title'                   => 'Element eliminat',
    'delete_confirmation_message'                 => "L'element ha estat eliminat correctament.",
    'delete_confirmation_not_title'               => "No s'ha pogut eliminar",
    'delete_confirmation_not_message'             => "Hi ha hagut un error. L'element potser no s'ha eliminat.",
    'delete_confirmation_not_deleted_title'       => "No s'ha pogut eliminar",
    'delete_confirmation_not_deleted_message'     => 'No ha passat res. El seu element està segur.',

    // DataTables translation
    'emptyTable'     => 'No hi ha dades disponibles a la taula',
    'info'           => "Mostrant registres _START_ a _END_ d'un total de _TOTAL_ registres",
    'infoEmpty'      => 'Mostrant 0 registres',
    'infoFiltered'   => '(filtrant de _MAX_ registres totals)',
    'infoPostFix'    => '',
    'thousands'      => ',',
    'lengthMenu'     => '_MENU_ elements per pàgina',
    'loadingRecords' => 'Carregant...',
    'processing'     => 'Procesant...',
    'search'         => 'Cercar: ',
    'zeroRecords'    => "No s'han trobat elements",
    'paginate'       => [
        'first'    => 'Primer',
        'last'     => 'Últim',
        'next'     => 'Següent',
        'previous' => 'Anterior',
    ],
    'aria' => [
        'sortAscending'  => ': activar per ordenar ascendentment',
        'sortDescending' => ': activar per ordenar descendentment',
    ],

    // global crud - errors
    'unauthorized_access' => 'Accés denegat - no té els permisos necessaris per veure aquesta pàgina.',
    'please_fix' => 'Si us plau, corregeixi els següents errors:',

    // global crud - success / error notification bubbles
    'insert_success' => "L'element ha estat afegit correctament.",
    'update_success' => "L'element ha estat modificat correctament.",

    // CRUD reorder view
    'reorder'                      => 'Reordenar',
    'reorder_text'                 => 'Arrossegar i deixar anar per reordenar.',
    'reorder_success_title'        => 'Fet',
    'reorder_success_message'      => "L'ordre ha estat guardat.",
    'reorder_error_title'          => 'Error',
    'reorder_error_message'        => "L'ordre no ha estat guardat.",

    // CRUD yes/no
    'yes' => 'Sí',
    'no' => 'No',

    // Fields
    'browse_uploads' => 'Pujar fitxers',
    'clear' => 'Netejar',
    'page_link' => 'Enllaç',
    'page_link_placeholder' => 'http://example.com/su-pagina',
    'internal_link' => 'Enllaç intern',
    'internal_link_placeholder' => 'Slug intern. Exemple: \'admin/page\' (sense cometes) per \':url\'',
    'external_link' => 'Enllaç extern',

];
