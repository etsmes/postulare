<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack\Base Language Lines
    |--------------------------------------------------------------------------
    */

    'registration_closed'  => "El registre d'usuaris està tancat.",
    'first_page_you_see'   => "La pàgina que veus després d'iniciar sessió",
    'login_status'         => "Estat de la connexió",
    'logged_in'            => '¡Ha iniciat sessió!',
    'toggle_navigation'    => 'Activar/desactivar la navegació',
    'administration'       => 'ADMINISTRACIÓ',
    'user'                 => 'USUARI',
    'logout'               => 'Sortir',
    'login'                => 'Iniciar sessió',
    'register'             => 'Crear usuari',
    'name'                 => 'Nom',
    'email_address'        => 'Correu',
    'password'             => 'Contrasenya',
    'confirm_password'     => 'Confirmar contrasenya',
    'remember_me'          => 'Recordar contrasenya',
    'forgot_your_password' => '¿Ha oblidat la seva contrasenya?',
    'reset_password'       => 'Restaurar contrasenya',
    'send_reset_link'      => 'Enviar enllaç para restaurar la contrasenya',
    'click_here_to_reset'  => 'Click aquí per restaurar la contrasenya',
    'unauthorized'         => 'No autoritzat.',
    'dashboard'            => 'Panell',
    'handcrafted_by'       => 'Realitzat per',
    'powered_by'           => 'Creat amb',
];
