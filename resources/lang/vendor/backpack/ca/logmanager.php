<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack\LogManager Language Lines
    |--------------------------------------------------------------------------
    */

      'log_manager'                 => "Log Manager",
      'log_manager_description'     => "Previsualitzar, descarregar i eliminar registres.",
      'actions'                     => "Accions",
      'delete_confirm'              => "Esteu segur que voleu suprimir aquest fitxer de registre?",
      'back_to_all_logs'            => "Tornar a tots els registres",
      'create_a_new_log'            => "Crear un nou registre",
      'date'                        => "Data",
      'delete'                      => "Eliminar",
      'delete_confirmation_title'   => "Fet",
      'download'                    => "Descarregar",
      'delete_error_title'          => "Error",
      'existing_logs'               => "Registres existents",
      'file_size'                   => "Mida del fitxer",
      'delete_cancel_title'         => "Correcte",
      'last_modified'               => "Última modificació",
      'logs'                        => "Logs",
      'preview'                     => "Vista prèvia",
      'delete_cancel_message'       => "El fitxer de NO ha estat eliminat.",
      'delete_error_message'        => "El fitxer de NO ha estat eliminat.",
      'delete_confirmation_message' => "El fitxer de ha estat eliminat.",
      'log_file_doesnt_exist'       => "L'arxiu de registre no existeix.",

];
