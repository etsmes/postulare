<?php

return [

    'Pagess'                => 'Pàgines',
	'Logs'                 	=> 'Logs',
	'Language_Files'       	=> "Fitxers d'idioma",
	'Languages'       		=> 'Idiomes',
	'File_manager'     		=> 'Administrador de fitxers',	
    'Permissions'     		=> 'Permisos',	

];
