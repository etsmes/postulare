<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Permission Manager Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Laravel Backpack - Permission Manager
    | Author: Lúdio Oliveira <ludio.ao@gmail.com>
    |
    */
    'name'                  => 'Nombre',
    'role'                  => 'Rol',
    'roles'                 => 'Roles',
    'roles_have_permission' => 'Roles que tienen este permiso',
    'permission_singular'   => 'permiso',
    'permission_plural'     => 'permisos',
    'user_singular'         => 'Usuario',
    'user_plural'           => 'Usuarios',
    'email'                 => 'Email',
    'extra_permissions'     => 'Permisos adicionales',
    'password'              => 'Contraseña',
    'password_confirmation' => 'Confirmación de contraseña',
    'user_role_permission'  => 'Permisos de rol de usuario',
    'user'                  => 'Usuario',
    'users'                 => 'Usuarios',

];
