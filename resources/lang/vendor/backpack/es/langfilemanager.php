<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack\LangFileManager Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD interface for lang files.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'active'                 => 'Activo',
    'cant_edit_online'       => 'Este archivo de idioma no puede ser editado online.',
    'code_iso639-1'          => 'Code (ISO 639-1)',
    'default'                => 'Defecto',
    'empty_file'             => 'Traducción no disponible.',
    'flag_image'             => 'Imagen de la bandera',
    'key'                    => 'Clave',
    'language'               => 'idioma',
    'language_name'          => 'Nombre del idioma',
    'language_text'          => ':language_name texto',
    'language_translation'   => ':language_name traducción',
    'languages'              => 'idiomas',
    'please_fill_all_fields' => 'Por favor, introduce todos los campos',
    'rules_text'             => "<strong>Aviso: </strong>No traducir palabras con prefijo de dos puntos (ej: ': number_of_items'). Ésos serán substituidos automáticamente con un valor apropiado. Si se traduce, deja de funcionar.",
    'saved'                  => 'Guardado',
    'site_texts'             => 'Texto del sitio',
    'switch_to'              => 'Cambiar a',
    'texts'                  => 'Textos',
    'translate'              => 'Traducir',
    'translations'           => 'Traducciones',
    'native_name'            => 'Nombre nativo',

];
