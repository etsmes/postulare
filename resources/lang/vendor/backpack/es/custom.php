<?php

return [

    'Pagess'                	=> 'P&#225;ginas',
	'Logs'                 	=> 'Logs',
	'Language_Files'       	=> 'Archivos de idioma',
	'Languages'       		=> 'Idiomas'	,
	'File_manager'     		=> 'Administrador de archivos',	
    'Permissions'     		=> 'Permisos',	

];
