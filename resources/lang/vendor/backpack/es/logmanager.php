<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack\LogManager Language Lines
    |--------------------------------------------------------------------------
    */

      'log_manager'                 => 'Log Manager',
      'log_manager_description'     => 'Previsualizar, descargar y eliminar registros.',
      'actions'                     => 'Acciones',
      'delete_confirm'              => '¿Está seguro de que desea eliminar este archivo de registro?',
      'back_to_all_logs'            => 'Volver a todos los registros',
      'create_a_new_log'            => 'Crear un nuevo registro',
      'date'                        => 'Fecha',
      'delete'                      => 'Eliminar',
      'delete_confirmation_title'   => 'Hecho',
      'download'                    => 'Descargar',
      'delete_error_title'          => 'Error',
      'existing_logs'               => 'Registros existentes',
      'file_size'                   => 'Tamaño del archivo',
      'delete_cancel_title'         => 'Correcto',
      'last_modified'               => 'Última modificación',
      'logs'                        => 'Logs',
      'preview'                     => 'Vista previa',
      'delete_cancel_message'       => 'El archivo de registro NO ha sido eliminado.',
      'delete_error_message'        => 'El archivo de registro NO ha sido eliminado.',
      'delete_confirmation_message' => 'El registro ha sido eliminado.',
      'log_file_doesnt_exist'       => "El archivo de registro no existe.",

];
