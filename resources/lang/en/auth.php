<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'Confirm' => 'Confirm',
    'failed' => 'These credentials do not match our records.',    
	'Forgot Your Password?' => 'Forgot Your Password?',
	'Login' => 'Login',
	'Logout' => 'Logout',	
	'Name' => 'Name',
	'Password' => 'Password',
	'Register' => "Registrarse",
	'Reset password' => "Reset password",
	'Send Password Reset Link' => "Send Password Reset Link",
	'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
	'Remember Me' => "Remember Me",
];
