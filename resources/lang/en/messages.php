<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

	'added successfully' => 'added successfully',
	'added successfully f' => 'added successfully',
    'Add'=>'Add',
	'Admin_panel' => "Admin panel",	
	'Appointment'=>'Appointment',
	'Appointments'=>'Appointments',	
	'Are you sure you want to delete this item?' => 'Are you sure you want to delete this item?',	
	'cancelled successfully' => 'cancelled successfully',
	'cancelled successfully f' => 'cancelled successfully',	
	'Can not delete an appointment with a reservation' => 'Can not delete an appointment with a reservation',
	'Cancel' => 'Cancel',	
	'Client' => 'Client',		
	'Comment_Client'=>'Customer comments',
	'Comment_Professional'=>'Professional comments',	
	'deleted successfully' => 'deleted successfully',
	'deleted successfully f' => 'deleted successfully',	
	'Delete' => 'Delete',	
	'Edit'=>'Edit',
	'End'=>'End',	
	'Home'=>'Home',		
	'Language'=>'Language',
	'My appointments'=>'My appointments',	
	'Professional' => 'Professional',		
	'Save' => 'Save',
	'selected successfully' => 'selected successfully',
	'selected successfully f' => 'selected successfully',			
	'Select' => 'Select',	
	'Service'=>'Service',
	'Settings'=>'Settings',
	'Show' => 'Show',
	'Start'=>'Start',	
	'stored successfully' => 'stored successfully',
	'stored successfully f' => 'stored successfully',	
	
    'Monday' => 'Monday',
	'Tuesday' => 'Tuesday',
	'Wednesday' => 'Wednesday',
	'Thursday' => 'Thursday',
	'Friday' => 'Friday',
	'Saturday' => 'Saturday',
	'Sunday' => 'Sunday',		
];

